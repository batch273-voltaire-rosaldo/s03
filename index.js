// in JS, classes can be created the "class" keyword and {}

// Naming convention for classes begins with uppercase characters
/*
    Syntax: 
        class NameOfClass {

        }
*/

// CLASS Student
class Student {

    constructor(name, email, grades) {
        this.name = name;
        this.email = email;
        this.gradeAve = undefined;
        this.passed = undefined;
        this.passedWithHonors = undefined;
        
        // Activity 
        // if (grades.length === 4) { 
        //     if (grades.every(grade => ((typeof grade === "number") && (grade >= 0 && grade <= 100)))) {
        //         this.grades = grades;
        //     } else {
        //         this.grades = undefined;
        //     };
        // } else {
        //     this.grades = undefined;
        // };
        if(grades.length === 4){
            if(grades.every(grade => grade >= 0 && grade <= 100)){
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        };

    }

    //Methods
    login() {
        console.log(`${this.email} has logged in`);
        // console.log(this);
        return this;
    }
    logout() {
        console.log(`${this.email} has logged out`);
        return this;
    }
    listGrades() {
        console.log(`${this.name}'s grades are: ${this.grades}`);
        return this;
    }
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        //update property
        // return sum / this.grades.length;
        this.gradeAve = sum / 4;
        // return object
        return this;
    }
    // Activity 2
    willPass() {
        this.passed = this.computeAve().gradeAve >= 85 ? true : false;
        return this;
    }
    willPassWithHonors() {

        if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}

        return this;
    }
}

// CLASS to represent a section of students
class Section {
    //every Section object will be instantiated 
    constructor(name) {
        this.name = name;
        this.students = [];
        this.honorStudents = undefined;
        this.honorsPercentage = undefined;
        this.sectionAve = 0;
    }

    // Method for adding student to this section
    addStudent(name, email, grades) {
        this.students.push(new Student(name, email, grades));

        return this;
    }

    countHonorStudents() {
        let count = 0;

        this.students.forEach(student => {
            if(student.computeAve().willPass().willPassWithHonors().passedWithHonors) {
				count++
			}
            // this.honorStudents = count;
        })
        this.honorStudents = count;
        return this;
    }

    computeHonorsPercentage() {
        this.honorsPercentage = (this.honorStudents / this.students.length  ) * 100;
        return this
    }
    sumAve() {
        let sum = 0;
        
        this.students.forEach(student => 
            sum += student.computeAve().gradeAve
            
        );
        this.sectionAve = sum/4;
            
        return this;
    }
}

//Activity
//CLASS for Grade level

class Grade {
    constructor(level) {

        if ((typeof level === "number") && (level >= 0)) {
            this.level = level;
        } else {
            this.level = undefined;
        };
       
        this.sections = [];
        this.batchAveGrade = undefined;
        this.batchMaxGrade = undefined;
        this.batchMinGrade = undefined;
        
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
    }

    // Method for adding section to this grade
    addSection(name) {
        this.sections.push(new Section(name));
        return this;
    }

    countStudents() {

        this.sections.forEach(section => {
            this.totalStudents += section.students.length;   
        });

        return this;
    }

    countHonorStudents() {
        
        this.sections.forEach(section => {
            this.totalHonorStudents += section.honorStudents;

        });
        return this;
    }

    computeBatchAve() {
        let sum = 0;
        
        this.sections.forEach(section => 
            sum += section.sumAve().sectionAve
        );
        this.batchAveGrade = sum/4;
        return this;
    }

    getBatchMinGrade() {

    }

    getBatchMaxGrade() {

    }

};

// const section1A = new Section('section1A');
// console.log(section1A);

//section1A.addStudent("John", "john@mail.com", [90, 90, 90, 90]);
//section1A.students[0]
//section1A.students[0].computeAve()

// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
// section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
// section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
// section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);
// console.log(section1A);

// ------------------For Testing ------------------//

// //instantiate new Grade object
const grade1 = new Grade(1);

// //2. add sections to this grade level
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');

// //save sections of this grade level as constants
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");

// grade1.sections.find(section => section.name === "section1A").addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
// // //populate the sections with students
// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
// section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
// section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
// section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

// section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
// section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
// section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
// section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

// section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
// section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
// section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
// section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

// section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
// section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
// section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
// section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);

section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]).countHonorStudents().computeHonorsPercentage().sumAve();
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]).countHonorStudents().computeHonorsPercentage().sumAve();
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]).countHonorStudents().computeHonorsPercentage().sumAve();
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]).countHonorStudents().computeHonorsPercentage().sumAve();

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]).countHonorStudents().computeHonorsPercentage().sumAve();
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]).countHonorStudents().computeHonorsPercentage().sumAve();
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]).countHonorStudents().computeHonorsPercentage().sumAve();
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]).countHonorStudents().computeHonorsPercentage().sumAve();

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]).countHonorStudents().computeHonorsPercentage().sumAve();
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]).countHonorStudents().computeHonorsPercentage().sumAve();
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]).countHonorStudents().computeHonorsPercentage().sumAve();
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]).countHonorStudents().computeHonorsPercentage().sumAve();

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]).countHonorStudents().computeHonorsPercentage().sumAve();
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]).countHonorStudents().computeHonorsPercentage().sumAve();
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]).countHonorStudents().computeHonorsPercentage().sumAve();
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]).countHonorStudents().computeHonorsPercentage().sumAve();


/*
let studentOne = new Student('John', 'john@email.com', [89, 84, 78, 88]);
let studentTwo = new Student('Joe', 'joe@email.com', [78, 82, 79, 85]);
let studentThree = new Student('Jane', 'jane@email.com', [87, 89, 91, 93]);
let studentFour = new Student('Jessie', 'jessie@email.com', [91, 89, 92, 93]);
*/

// Activity 1 - Quiz

// 1. What is the blueprint where objects are created from?
// Ans. : Class
// 2. What is the naming convention applied to classes?
// Ans. : in Pascal case
// 3. What keyword do we use to create objects from a class?
// Ans. : keyword = "new"
// 4. What is the technical term for creating an object from a class?
// Ans. : Instantiate
// 5. What class method dictates HOW objects will be created from that
// class?
// Ans. : constructor

//Activity 2

// 1. Should class methods be included in the class constructor?
// Ans. : No
// 2. Can class methods be separated by commas?
// Ans. : No
// 3. Can we update an object’s properties via dot notation?
// Ans. : Yes
// 4. What do you call the methods used to regulate access to an object’s properties?
// Ans. : 
// 5. What does a method need to return in order for it to be chainable?
// Ans. : "return this"


/*
    Mini-Activity:
        Person class

        name,
        age - (should be a number and must be greater than or equal to 18, otherwise set the property to undefined),
        nationality,
        address

        Instantiate 2 new objects from Person class. person1 and person2

        Log both obj in the console.

*/

class Person {
    constructor(name, age, nationality, address) {
        
        this.name = name;
        
        this.age = ((typeof age == "number" && age >= 18) ? age : undefined );
        this.nationality = nationality;
        this.address = address;
    }
}

// let person1 = new Person('Tomas', 25, 'Filipino', 'Bulacan');
// let person2 = new Person('John', 17, 'Swedish', 'Sweden');

